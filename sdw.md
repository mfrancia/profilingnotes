## Spatial datawarehouse

### [Semantic Trajectory Modeling and Analysis](http://www.uhasselt.be/Documents/datasim/Papers/Semantic-Trajectories-Modeling-and-Analysis.pdf)

Thanks to city information, spatio-temporal coordinates can be replaced with
street and crossing names, or with names of places of interest, such as shops,
restaurants, and museums. Information about ongoing events (e.g. concerts,
fairs) enables e.g. traffic monitoring applications to differentiate normal
traffic conditions from exceptional traffic conditions. We generically call
contextual data repository the external data sources (e.g., application
databases, geo databases, web pages) that provide contextual data. 

Adding knowledge to raw trajectories is known as a **semantic enrichment**
process (see Section 5). Enrichment conveys the idea that existing data is
complemented with additional data, called annotations. An annotation here is
any additional data that is attached either to a trajectory as a whole or to
some of its subparts. For example, recording the goal of a person's trip to
Paris (e.g., business, tourism) is an annotation at the trajectory level (i.e.,
holding one value per trajectory). 

Another way of enhancing the knowledge on trajectories is to superimpose a
structure of homogeneous segments (with respect to given segmentation criteria)
that are meaningful for the application. Such homogeneous segments are called
episodes [Mountain and Raper 2001], where an episode is defined as a maximal
sub-sequence of a trajectory such that all its spatio-temporal positions comply
with a given predicate. 

A trajectory may be segmented in various ways corresponding to whatever
segmentation criteria are of interest to the application at hand. A popular
segmentation criterion is stillness versus movement, which generates two kinds
of alternating episodes, called stops and moves.

A **trajectory behavior** (behavior, in short) is a set of characteristics that
identifies a peculiar bearing of a moving object or of a set of moving objects.
The behavior is defined by a predicate that says if a given trajectory (or a
given set of trajectories) shows the behavior.

The predicate that defines a behavior may rely on the spatio-temporal
characteristics of the trajectories, e.g. speed for the Speeding behavior that
characterizes vehicles speeding above the speed limit, or the Begin and End
positions for the Meet behavior).  It may also rely on the semantic information
conveyed via the annotations and the contextual data that are linked through
spatio-temporal relationships to the trajectories. This is the case, for
example, for the Tourist behavior.  A trajectory can show several behaviors.
For instance a trajectory can show both the Speeding and the Tourist behaviors,
and simultaneously be part of a group of trajectories showing the Meet
behavior. For each behavior the predicate relies on different characteristics.


### [A general framework for trajectory data warehousing and visual OLAP](http://download.springer.com/static/pdf/742/art%253A10.1007%252Fs10707-013-0181-3.pdf?originUrl=http%3A%2F%2Flink.springer.com%2Farticle%2F10.1007%2Fs10707-013-0181-3&token2=exp=1493796764~acl=%2Fstatic%2Fpdf%2F742%2Fart%25253A10.1007%25252Fs10707-013-0181-3.pdf%3ForiginUrl%3Dhttp%253A%252F%252Flink.springer.com%252Farticle%252F10.1007%252Fs10707-013-0181-3*~hmac=9d348b1f99d1f1e6bc51ee7b49cc225908ed2d92d03ab90f0e3c0ae38e41922c)

Among the others, we introduce a novel measure visits , denoted by V , which
counts the number of times the various trajectories visits a given granule.

An element of interest for V is the fact that it provides an approximation of
measure presence , denoted by P , which counts the number of distinct
trajectories occurring in a spatio-temporal granule.

"Which is the number of buses per hour in the morning of a given day in the
neighbourhoods of a given district? Show its temporal evolution using a
temporal granularity of half an hour", or "From which district does a great
number of cars leave in the morning? And at what hour? Is there a flow
exiting/entering the town? Which are the main differences in the traffic
between the working days and the week-end?"

A first choice regards the TDW base granularity , which is a collection of ele-
ments called base granules , obtained by partitioning both the spatial and
temporal dimensions. Informally, a granule can be defined as a contiguous
spatial region during a given time interval. According to the different
scenarios, the granule-based decomposition can generate a regular or irregular
tessellation of the space domain during a given time interval. For example, in
the vessel scenario, the North Adriatic Sea can be partitioned in squared
regular areas whereas in the road traffic scenario, base granules can be
associated with segments of the road network.

Intra-granule: These facts model events that are related to a single base
granule concerning a certain object group.

Inter-granule: These facts model events that are related to pairs of granules
and are concerned with a specific object group.

Note that the measure cross is interesting only for adjacent granules (for non-
adjacent granules it is invariably 0). However, in general, inter-granule facts
can model events which are meaningful for all pairs of granules. An example
could be the origin-destination measure, which, for any pair of granules,
represents the number of trajectories starting from the first and ending into
the second granule.  Clearly, the presented measures are not an exhaustive
collection, but they correspond to a set of common measures which we found
interesting and useful in different scenarios.

### [St-Toolkit: A Framework for Trajectory Data Warehousing](https://agile-online.org/conference_paper/cds/agile_2011/contents/pdf/shortpapers/sp_110.pdf)



### [Logical Representation of a Conceptual Model for Spatial Data Warehouses](http://download.springer.com/static/pdf/439/art%253A10.1007%252Fs10707-007-0022-3.pdf?originUrl=http%3A%2F%2Flink.springer.com%2Farticle%2F10.1007%2Fs10707-007-0022-3&token2=exp=1493735993~acl=%2Fstatic%2Fpdf%2F439%2Fart%25253A10.1007%25252Fs10707-007-0022-3.pdf%3ForiginUrl%3Dhttp%253A%252F%252Flink.springer.com%252Farticle%252F10.1007%252Fs10707-007-0022-3*~hmac=e6e465244f8669e0a50a9004a7cbdac96054d453851656e61a64044a71575d3a)

### [Tesi dottorato](http://www.unive.it/media/allegato/DIP/SAmbInfStat/Dottorati/TesiInf/TD-2012-2-Leonardi.pdf)

### [PostTrajectory](https://github.com/awarematics/posttrajectory) - Last update (January 2017)

PostTrajectory is a trajectory and moving objects management system developed
on PostgreSQL/PostGIS.

### [pg-trajectory](https://www.researchgate.net/publication/309571622_Pg-Trajectory_A_PostgreSQLPostGIS_Based_Data_Model_for_Spatiotemporal_Trajectories) - Last update 2016-10-05

[website](http://pg-trajectory.dmlab.cs.gsu.edu/)

### Related questions

- [Using PostGis for a large trajectory dataset](https://gis.stackexchange.com/questions/50016/using-postgis-for-a-large-trajectory-dataset)
