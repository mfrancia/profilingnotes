## Patent [Location based profiling 2002](https://www.google.com/patents/US20020111172)

The present invention discloses a method and system for profiling **a
subscriber** (thus does not address profiling of group of people) based on his
activities and locations traveled
[[Example]](https://patentimages.storage.googleapis.com/US20020111172A1/US20020111172A1-20020815-D00010.png).
The subscriber activities and locations are observed and processed to develop a
profile of the subscriber that may include demographics, psycho-graphic make-up
and activity pattern of the subscriber.

The method includes receiving data related to a location of the subscriber and
retrieving data characterizing the location. The profile is generated based
upon the subscriber location data and the characteristics of the subscriber
location data.

### Background
The advent of wireless communications provides the ability for users to
communicate from a moving location.

Many mobile transactions require the location of the user be known. For
example, services such as the Emergency 911 System, require that the location
be known so that the Emergency call can be routed to the appropriate call
center.

Wireless phones have no fixed position, therefore without communication from
the caller to identify their present location an appropriate dispatch
(emergency response team to the correct location) cannot be made.

An alternative technology that is being developed places global positioning
satellite (GPS) functionality on a chip that is placed in the wireless device.

Another example of the expanding use of this technology is the deployment of
vehicle navigation systems developed for the consumer market. These systems are
generally found to be of two types. The first type is comprised of a GPS unit,
a compass, a map database, and a user interface (visual and/or with a voice
interface).

It has been through a separate set of developments that an advertising
supported business model can be now applied to wireless communications. An
article from the Wall Street Journal Interactive Edition, "Dial the Web:
MobileID Invests in CellPhone Search Engine", printed from the World Wide Web
site http://interactive.wsj.com/archive/retrieve.cgi?id=SB964645721139838971
on Jul. 7, 2000, discloses just such a business model.

Pure proximity based services are not necessarily of significant value. It may
be that while I am in close proximity to a McDonalds restaurant, and that
McDonalds is currently running a marketing campaign that includes a coupon
entitling me to a discount, and that I am equipped with a device capable of
determining my location and that my service provider has agreed to deliver the
marketing materials to its subscribers, I may never have eaten at a McDonalds
nor might ever intend to. Sending me the advertisement would be both a waste of
McDonalds time as well as mine. The service provider might irritate me with
irrelevant materials to the point where I unsubscribe from their service.

Thus, there is a need for a system and method of generating a profile of a
subscriber based on location that could be used to target advertisements to the
subscriber.

### Description

The observed activities (step 520) are categorized by **analyzing the time
data, frequency, route, etc. associated with the subscriber** 210. For example,
if Monday through Friday mornings between approximately 8:00 AM and 9:00 AM the
subscriber takes roughly the same path between Doylestown, Pa. and
Philadelphia, Pa., an analogy can be made that the subscriber 210 is commuting
to work. Another example, may be that if on Saturday mornings the subscriber
goes to numerous locations within town, an analogy can be made that the
subscriber 210 is running errands. As one of ordinary skill in the art would
recognize, there are rules that could be applied that could classify the type
of activities that a subscriber 210 was performing. The classification may be
in the form of a probability. That is, depending on the time, the location and
other features, a determination might be made that there is an 80% chance that
the activity the subscriber 210 is partaking in (or is about to partake in) is
an errand.

Obviously if the subscriber 210 doesn't like coffee then delivering the
subscriber 210 an advertisement for a coffee shop is probably of little or no
value. Thus, in a preferred embodiment, the activity/route profile is enhanced
by incorporating the subscriber profile (discussed in more detail below). That
is, the activity/route profile would be enhanced by identifying the entities on
a predicted route that would be of interest to the subscriber 210.

The subscriber activity profile may be used to predict the activity to which
the subscriber is about to participate. In one embodiment, each activity and
sub-activity is related to the season or time of the year, to the day of the
week and time of the day and also a path through the location area that the
subscriber takes to perform the activity. Such mapping of the activity in space
and time allows the system to generate an activity pattern for each subscriber
that may then be used in predicting the activities of the subscriber.

[This figure](https://www.google.com/patents/US20020111172) illustrates an
exemplary subscriber profile that identifies a probability that a subscriber
210 falls within a certain demographic category such as an age group, gender,
household size, or income range. According to one embodiment, the subscriber
profile includes interest categories that may be organized according to broad
areas such as music, travel, and restaurants. Examples of music interest
categories include country music, rock, classical, and folk.  Examples of
travel categories include travels to another state more than twice a year, and
travels by plane more than twice a year.

Although this invention has been illustrated by reference to specific
embodiments, it will be apparent to those skilled in the art that various
changes and modifications may be made, which clearly fall within the scope of
the invention.

### What is missing?

0. Group profiling
0. ``Shop'' profiling: we know the profile of a subject, we don't know the
profiles of possible customers
0. Prediction of future places: we don't know which places person X will visit
    - In addition, if these places are in Bologna, where will person X go if she moves in Milan?

## [Introduction to Mobile Trajectory Based Services: A New Direction in Mobile Location Based Services](http://download.springer.com/static/pdf/44/chp%253A10.1007%252F978-3-642-03417-6_39.pdf?originUrl=http%3A%2F%2Flink.springer.com%2Fchapter%2F10.1007%2F978-3-642-03417-6_39&token2=exp=1493713949~acl=%2Fstatic%2Fpdf%2F44%2Fchp%25253A10.1007%25252F978-3-642-03417-6_39.pdf%3ForiginUrl%3Dhttp%253A%252F%252Flink.springer.com%252Fchapter%252F10.1007%252F978-3-642-03417-6_39*~hmac=eb97ecf9fb8fead8de8c85336d5ca4509b3ace225e11b04e9216ea53390bf169)

### Description

0. Pull and push services (query vs alert/notification)
0. GIS

## [Measuring User Similarity with Trajectory Patterns: Principles and New Metrics](http://download.springer.com/static/pdf/77/chp%253A10.1007%252F978-3-319-11116-2_38.pdf?originUrl=http%3A%2F%2Flink.springer.com%2Fchapter%2F10.1007%2F978-3-319-11116-2_38&token2=exp=1493719313~acl=%2Fstatic%2Fpdf%2F77%2Fchp%25253A10.1007%25252F978-3-319-11116-2_38.pdf%3ForiginUrl%3Dhttp%253A%252F%252Flink.springer.com%252Fchapter%252F10.1007%252F978-3-319-11116-2_38*~hmac=6d1720b09c322adbfa012b67dd8e143cfda8e8d7616a18b2ef5c6090748d21d2)

### GOAL

In this paper, we identify and define the basic principles that hold when
measuring user similarity based on (semantic) trajectory patterns. These
principles enable us to re-evaluate existing metrics and discuss their
insufficiency in capturing user similarity.

### Description

One method to identify users with similar movements is to construct and compare
their mobility profiles which are composed of their trajectory patterns [3].
Intuitively, a trajectory pattern is a sequence of places of interest which a
user frequently visits.  The frequency by which the pattern is followed is
called its support value. For instance, every morning Pierre, a student in
Oxford travels by train from his home to Oxford from which he walks to Trinity
College. This daily routine can be described as a trajectory pattern: Home >
Oxford station > Trinity College.

**Location semantic similarity**.  The consideration  of  the  functionalities
of places  can  reveal more about users' similar hobbies. For instance, two
users who live in different cities both like reading. According to their
mobility, we cannot find their similarity be- cause they go to different book
stores. How ever, when considering the functionalities of places, e.g., book
store, we will be able to discover their common interest. We call the
functionalities of places location semantics.

### Related works

- [On the semantic annotation of places in location-based social networks](http://dl.acm.org/citation.cfm?id=2020491)

## [Mining mobility user profiles for car pooling](http://dl.acm.org/citation.cfm?id=2020591)

### Goal

In this paper we focus on a car pooling application aimed at identifying pairs
of users that could most likely share their vehicle for one or more of their
routine trips. The service might be deployed as a system that provides
pro-active suggestions to facilitate the matching process, without the need for
the user to explicitly describe (and update) the trips of interest

### Description

The daily mobility of each user can be essentially summarized by a set of
single trips that the user performs during the day. When trying to extract a
mobility profile of users, our interest is in the trips that are part of their
habits, there- fore neglecting occasional variations that divert from their
typical behavior

Our objective is to use the set of trips of an individual user to find his/her
routine behaviors. We do this by grouping together similar trips based on
concepts of spatial distance and temporal alignment, with corresponding
thresholds for both the spatial and temporal components of the trips. In order
to be defined as routine, a behavior needs to be supported by a significant
number of similar trips.

**GPS and SGM data**. User profiles can also be extracted and compared on the
basis of mobile phone network traces that are commonly (and massively)
available from telecom operators. It is therefore natural to wonder whether the
loss in spatial and tempo- ral precision and completeness that characterizes
this kind of data can compromise the current usability of our methodology.  The
general methodology introduced so far in this paper can be directly applied to
this kind of data and en- ables us to perform exactly the same kind of analysis
on both GPS and GSM data and compare the results.
