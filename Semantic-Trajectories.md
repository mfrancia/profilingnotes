# Semantic trajectories 
Based on the large availability of **GPS** sensors data the new objectives of **spatio-temporal** data analysis are [Semantic Trajectories Modeling and Analysis]:
  - constructing *trajectories*
  - enriching trajectories to enable *interpretation*
  - *data mining* to extract *behavioral patterns*
  
The new approach devised to provide meaningful knowledge about movement is named **semantic trajectories**
This approach can be organized in
  - basic concepts and terminology
  - trajectory reconstruction
  - semantic enrichment
  - behavior knowledge extraction

## Basic concepts
The trajectory is the *movement of an object in geographical space over a period of time*.
A **movement track** consists in the temporal sequence of pairs (instant, point), but we can add additional data such as *instant speed* or *stillness*, *acceleration*, *direction*, *rotation*. The data provided by the sensor are called **raw data**
From the movement track we can extract only the segment of the movement we are interested in. The segment is identified with a **begin** and an **end** points and this is the trajectory[Spaccapietra et al. 2008]. We can extract multiple relevant trajectories from a single movement track.
A **raw trajectory** is a trajectory extracted from a **raw movement track** containing only raw data for it's interval.
The movement track contains only **sampled** points, while the movement is **continuous**. This can be considered as an *approximation* of the real movement.
However sometimes we have *greater* gaps between points and we differentiate between two kind of gap [Vazirgiannis and Wolfson 2001]:
  - a **hole** is when the information is **accidentally** missing for example example due to a malfunction.
  - a **semantic gap** is when the information is **intentionally** missing for example due to GPS shutdown by someone.

The *holes* may sometimes be filled by interpolation, the *semantic gaps* instead are not filled because is intentionally missing.

Adding **contextual data repository** to *raw trajectory* we can transform them in a **semantic trajectory**, this process is called **semantic enrichment**. The data sources can be for example geo databases or web pages.

A *semantic trajectory* is a trajectory that has been enhanced with annotations and segmentations. It's defined as a tuple [Spaccapietra and Parent 2011]

## Trajectory related Behaviors

Analyzing the *similarities* among the trajectory, identify the *outliers*, classifying trajectories may form a set of distinguishing characteristics, these are called patterns or **behavior**. [Laube 2009]

A **trajectory behavior** is a set of characteristic that identify a peculiar bearing of a moving object.

An important characteristics of behaviors is whether they apply to a single trajectory or a *group*.

An **individual trajectory behavior** is a trajectory behavior whose predicate p(T) bears to a trajectory (T)

a **collective trajectory behavior** is a trajectory behavior whose predicate p(S) bears to a non empty set of trajectories(S), for example **Flock behavior**


 