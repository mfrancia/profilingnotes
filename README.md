# Modeling social behavior from trajectory data
## Testo
Il crescente utilizzo di device che registrano la posizione dell’utente (es.
mediante GPS) ha reso disponibile una grossa massa di dati di traiettoria. Come
conseguenza, i ricercatori hanno concentrato la propria attenzione nello
sviluppo di soluzioni finalizzate all’analisi delle traiettorie.  Tali
soluzioni rientrano nell’area del data mining (es. clustering, classificazione)
e sono finalizzate a identificare luoghi rilevanti (hotspot), profili di
movimento, comunità, ecc. Sebbene siano disponibili già diverse soluzioni per i
problemi di base molto può ancora essere fatto per migliorare l’efficienza e
l’efficacia delle tecniche. Sul lato dell’efficienza va capito se l’utilizzo di
tecniche di modellazione dei dati ad hoc e l’utilizzo di sistemi propri dei Big
Data possa rendere possibile l’analisi di moli di dati di grandi dimensioni e
in tempo reale. Sul lato dell’efficacia molto può ancora essere fatto per
accoppiare le traiettorie con i dati semantici disponibili sulle mappe digitali
al fine di inferire comportamenti e profili degli utenti. In particolare,
nell’attuale letteratura il focus dell’analisi è principalmente la traiettoria
mentre poco si è fatto per effettuare una profilazione dell’utente basata sulle
informazioni estrapolabili dalle traiettorie. Un altro aspetto che merita di
essere investigato è la performance dei sistemi quando la qualità delle
informazioni posizionali degrada per esempio passando dalla precisa ubicazione
fornita da un GPS a quella approssimata definita in base all’antenna del
sistema di telefonia mobile a cui un device è collegato.

## Un po' di analisi

Ci troviamo in uno scenario di pervasive computing. In cui quasi tutte le
persone possiedono almeno un dispositivo che consente di tracciare la loro
posizione.

Informazioni di questo tipo possono essere sfruttate per fare il profiling di
chi si sta muovendo. Per profiling intendiamo la capacità di inferire il
comportamento dei partecipanti al sistema, indagandone i loro usi e costumi a
partire dai movimenti. Ad esempio: la persona X dorme nell'area Y, lavora
nell'area Z e frequenta usualmente i luoghi A, B, C. Quindi possiamo inferire
lo stato sociale di tale persone, e magari aspetti ancora più personali (es
frequenta centri sociali, la libreria, universita', etc).

Gli approcci esistenti in letteratura si concentrano più sulla nozione di
traiettoria. Noi vogliamo invece concentrarci non solo sul mining di
traiettorie (vedi clustering, classificazione), ma vogliamo definire quali
siano gli hotspot frequentati dalla persone.

Sicuramente questo progetto presenta challenge a più livelli. In sostanza
possiamo definire 4 livelli gerarchici:

0. Dati geolocalizzati, ovvero posizione geografica connessa ad un time stamp.
  0. Come memorizziamo dati all'interno di un GIS. Come individuiamo i nostri
componenti: aree, poligoni o singoli punti.
  0. Come gestiamo una quantità enorme di dati: *maritime trjectories,
  18 million points per day*
  0. Come gestiamo l'accuratezza dei dati, il GPS ha una accuratezza molto
  variabile. E se dovessimo gestire dati provenienti da celle GSM? E rete WIFI?
  E RFID?
  0. Come gestiamo l'interrogazione in tempo reale di questi dati. Possiamo
  fare analisi online?
0. Unire questi time stamp in traiettorie percorse dai componenti del sistema
  0. Discovery of POIs (clustering, areas with high densities of points, indoor
  places in which we lose GPS signal)
  0. Mobility profile (how subjects move)
  0. Trajectory interaction (understanding how subjects interact together)
  0. Data segmentation
  0. Outlier detection
0. Creare una traiettoria strutturata. Individuare gli hotspot, punti salienti,
in cui la persona X trascorre diverso tempo (esempio sulla base della densità
di punti presenti in una certa area - es clustering di punti vicini facenti
parte di una traiettoria; individuare i punti al coperto in cui il gps perde
sengale per molto tempo, etc).
0. Creare una traiettoria semantica, cercando di associare ad ogni hotspot un
punto di interesse all'interno della città (biblioteca, università, uffici).
Ad esempio questo è possibile incrociando la posizione dei punti salienti con
dati da google maps (sfruttando informazioni quali orari di apertura,
vicinanza, tipologia di struttura, etc.). Inoltre è rilevante la possibilità
doi incrociare i dati con informazioni appartenti a dati ISTAT, in modo da
arricchire le struttura semantica delle traiettorie con layer di conoscienza
crescente.
0. Creazione di una traiettoria social, in cui andiamo a definire i veri e
propri usi e custumi della singola persona (o gruppi di persone). **user habits
and behavior analysis**.

### Join di traiettorie
Oltre alla gestione di singole traiettorie, è molto interessante capire come
le traiettorie possano essere raggruppate per inferire profili comuni a più
persone.
0. Trajectory join, Hadoop map reduce
0. T pattern, trajectories visiting the same places in a similar travel time
0. Clustering (source & destination, route, direction). Group of trajectory
that share a sub-trajectory or being close w.r.t. A given metric.
0. Analysis of movement patterns: Flock, convey, swarm

### Questions
- Big data, how big? Do we have enough data?
- Do we know the purpose of the travel? Can we infer the nature of the
	movement? If a person is close to the point X, which are plausible POI near
	X?
- Data accuracy (GPS vs GSM vs WIFI vs etc)
- Data frequency, if we sample data every `t` seconds, we lose information
	about what happens in between those `t ` seconds
- Data integration from heterogeneous services (google APIs, mobile
	applications)

### Further development
- Prediction of places
  - Markov models, trajectory patterns
- Community interaction
- Relationship between similar profiles
- Integrating trajectory and social data, inferring information about social
	networks
- Understanding whether a product will be successful in a given area
- Understanding why a product is successful in an area w.r.t. another


### Definizioni
- Con il termine **Data Mining** si intende un insieme di tecniche e strumenti
usati per esplorare grandi database, con lo scopo di individuare/estrarre
conoscenze significative, in modo da renderle disponibili ai processi
decisionali.  Estrazione complessa di informazioni implicite,  precedentemente
sconosciute e potenzialmente utili dai dati. Esplorazione e analisi, per mezzo
di sistemi automatici e  semi-automatici, di grandi quantità di dati al fine di
scoprire pattern significativi.
- **Pattern**: modello di dati sintetico e ricco di semantica.
  - Valido, comprensibile, non noto a priori
- **Classificazione**  consentono di derivare un modello per la classificazione
di dati
  secondo un insieme di classi assegnate a priori
- **Clustering**: raggruppa gli elementi di un insieme, a seconda delle loro
caratteristiche, in classi non assegnate a priori
- **Alberi decisionali**:  individuare cause che portano ad un evento
- **Regole associative**: regole di implicazione logica per individuare
affinità
- CRISP-DM: analisi dominio applicativo, analisi dati, preparazione dati,
analisi di uno o più modelli,
valutazione del modello, deployment
- Dati: qualitativi - ordinali, nominale, quantitativi - di intervallo, di
rapporto

### Bibliography
- [OT] [User profiling based on multiple aspects of activity in a computer system](https://www.infona.pl/resource/bwmeta1.element.baztech-a754dcd5-0886-4b1e-8646-5e962c168041)
		- The paper concerns behavioral biometrics, specifically issues related to
			the verification of the identity of computer systems users based on user
			profiling. The profiling method for creating a behavioral profile based
			on multiple aspects of user activity in a computer system is presented.
			The work is devoted to the analysis of user activity in environments with
			a graphical user interface GUI.
- [Profiling Moving Objects by Dividing and Clustering Trajectories Spatiotemporally](http://ieeexplore.ieee.org/abstract/document/6399468/)
		- Therefore, given a set of collected trajectories spreading in a bounded
			area, we are interested in discovering the typical moving styles in
			different regions of all the monitored moving objects. These regional
			typical moving styles are regarded as the profile of the monitored moving
			objects, which may help reflect the geoinformation of the observed area
			and the moving behaviors of the observed moving objects.
- [**Introduction to Mobile Trajectory Based Services: A New Direction in Mobile Location Based Services**](http://link.springer.com/chapter/10.1007/978-3-642-03417-6_39)
	- Pull and push services (query vs alert/notification) and GIS
- [NA] [Trajectory Indexing and Retrieval](http://link.springer.com/chapter/10.1007/978-1-4614-1629-6_2)
- [Trajectory Based Activity Discovery](http://ieeexplore.ieee.org/abstract/document/5597122/)
	- This work addresses these problems by presenting a novel framework  that
		links  the  basic  visual  information  (i.e., tracked objects) to the
		discovery and recognition of activities
- [A PROFILING BASED APPROACH TO SAFETY SURROGATE DATA COLLECTION](http://onlinepubs.trb.org/onlinepubs/conferences/2011/RSS/1/Peesapati,L.pdf)
	- The  current study  develops  a  comprehensive  methodology  for  obtaining
		objective  and  detailed  vehicle profile data from video.  
- [OT] [Trajectory Analysis and Semantic Region Modeling Using Nonparametric Hierarchical Bayesian Models](http://link.springer.com/article/10.1007/s11263-011-0459-6)
	- Unsupervised trajectory analysis and semantic region modeling in
		surveillance settings. In our approach, trajectories are treated as
		documents and observations of an object on a trajectory are treated as
		words in a document.
- [Measuring User Similarity with Trajectory Patterns: Principles and New Metrics](http://link.springer.com/chapter/10.1007/978-3-319-11116-2_38)
	- In this paper, we identify and define the basic principles that hold when
		measuring user similarity based on (semantic) trajectory patterns. These
		principles enable us to re-evaluate existing metrics and discuss their
		insufficiency in capturing user similarity.
- [OT] [Abnormal Behavior Detection Using Trajectory Analysis in Camera Sensor Networks](http://journals.sagepub.com/doi/full/10.1155/2014/839045)
- [OT] [Motion-based behaviour learning, profiling and classification in the presence of anomalies](http://www.sciencedirect.com/science/article/pii/S003132030900154X)
	- Analysis of trajectories in visual surveillance
- [**Mining mobility user profiles for car pooling**](http://dl.acm.org/citation.cfm?id=2020591)
	- The core contribution of this paper consists in a methodology for mobility
		analysis, aimed in particular to match users based on their individual
		mobility behaviours.
- [Analyzing developmental trajectories: A semiparametric, group-based approach.](http://citeseerx.ist.psu.edu/viewdoc/download;jsessionid=EE866EE99132EDAE2BF2F93682E326CF?doi=10.1.1.119.8625&rep=rep1&type=pdf)
- [Navigation skills based profiling for collaborative wheelchair control](http://ieeexplore.ieee.org/abstract/document/5980249/)
- [Mining Mobility Behavior from Trajectory Data](http://ieeexplore.ieee.org/abstract/document/5282986/)
- [The use of pervasive sensing for behaviour profiling — a survey](http://www.sciencedirect.com/science/article/pii/S1574119209000583)
- [Position-dependent alternative splicing activity revealed by global profiling of alternative splicing events regulated by PTB](http://www.nature.com/nsmb/journal/v17/n9/abs/nsmb.1881.html)
- [Profiling the non-users: Examination of life-position indicators, sensation seeking, shyness, and loneliness among users and non-users of social network sites](http://www.sciencedirect.com/science/article/pii/S0747563212001446)
- [Profiling of the public-service élite: A demographic and career trajectory survey of deputy and assistant deputy ministers in Canada](http://onlinelibrary.wiley.com/doi/10.1111/j.1754-7121.2007.tb02209.x/full)
- [GPS trajectory feature extraction for driver risk profiling](http://dl.acm.org/citation.cfm?id=2030091)
- [Estimation of Mobile Trajectory in a Wireless Network: A Basis for User's Mobility Profiling for Mobile Trajectoy Based Services](http://ieeexplore.ieee.org/abstract/document/5210950/?reload=true)
- [A Survey of Vision-Based Trajectory Learning and Analysis for Surveillance](http://ieeexplore.ieee.org/abstract/document/4543858/)
- [Context-based trajectory descriptor for human activity profiling](http://ieeexplore.ieee.org/abstract/document/6974283/)
- [Quantitative analysis of simple and interconnected systems: Stability, boundedness, and trajectory behavior](http://ieeexplore.ieee.org/abstract/document/1083119/)
- [Location based profiling - US 20020111172 A1](https://www.google.com/patents/US20020111172)
- [OT] [Data analysis system for tracking financial trader history and profiling trading behavior - US 20020004774 A1](https://www.google.com/patents/US20020004774)
- [Profiling, What-if Analysis, and Cost-based Optimization of MapReduce Programs](http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.447.5193&rep=rep1&type=pdf)

### Democracy and big data

- https://www.theguardian.com/commentisfree/2017/mar/06/big-data-cambridge-analytica-democracy

- http://heinonline.org/HOL/LandingPage?handle=hein.journals/nwteintp11&div=20&id=&page=

- http://agris.fao.org/agris-search/search.do?recordID=US201300669441

- http://www.tandfonline.com/doi/abs/10.1080/17538947.2015.1008214

- https://books.google.it/books?hl=it&lr=&id=9lO3BgAAQBAJ&oi=fnd&pg=PP1&dq=big+data+democracy&ots=s83dr-TwAs&sig=2S1HYhpVCMC_NhrmkvQFKE_p2d4#v=onepage&q=big%20data%20democracy&f=false

- https://books.google.it/books?hl=it&lr=&id=Bq7uk1xgqUEC&oi=fnd&pg=PR9&dq=big+data+democracy&ots=b_Uok63WQv&sig=JGMa_gQi-b-inAaWyNvEcB23MVo#v=onepage&q&f=false

- http://www.opendataimpacts.net/report/wp-content/uploads/2010/08/How-is-open-government-data-being-used-in-practice.pdf

- https://scholar.google.it/scholar?start=10&q=big+data+democracy&hl=it&as_sdt=0,5

- https://books.google.it/books?hl=it&lr=&id=GfOICwAAQBAJ&oi=fnd&pg=PP1&dq=big+data+democracy&ots=pczdMTXgUT&sig=K4UPBf3I5bBLFNWa_S6pA6tsPyw#v=onepage&q=big%20data%20democracy&f=false

- https://books.google.it/books?hl=it&lr=&id=PSsGAQAAQBAJ&oi=fnd&pg=PA1&dq=big+data+democracy&ots=x9zoXu9mGu&sig=lEnuOE8C8Wgumm2L7mh45zM_UfI#v=onepage&q=big%20data%20democracy&f=false

- http://www.academia.edu/download/32970305/FROM_BIG_DATA_TO_BIG_IMPACT.pdf

- http://www.nature.com/nsmb/journal/v17/n9/abs/nsmb.1881.html
